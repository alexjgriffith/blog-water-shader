(local lg love.graphics)
(local assets {} )
(local scale 4)

(local map
       "
CCCCCCCCCCCC
CCCCCCCCCCCC
CCCCCCCCCCCC
CCD330CCCCCC
CC899FCCCCCC
CCCCCCCCCCCC
CCCCCCCCCCCC
CCCCCCCCCCCC
CCCCCCCCCCCC
")

(macro incf [x] `(do (set ,x (+ ,x 1)) ,x))
(macro incfb [x b] `(do (set ,x (+ ,x 1)) (when (> ,x ,b) (set ,x 1)) ,x))
(macro set1 [x] `(do (set ,x 1) ,x))

(fn parse-map [m]
  (fn hex-to-dec [value]
    (?. {:0 0 :1 1 :2 2 :3 3 :4 4 :5 5 :6 6 :7 7 :8 8
         :9 9 :A 10 :B 11 :C 12 :D 13 :E 14 :F 15}
        value))
  (local parsed {})
  (var col-number 1)
  (var row-number 1)
  (each [row (string.gmatch map "(%w+)" )]
    (tset parsed row-number {})
    (each [value (string.gmatch row ".")]
      (tset parsed row-number col-number (+ 1 (hex-to-dec value)))
      (incf col-number))
    (set1 col-number)
    (incf row-number))
  parsed)

(fn make-quad-list-4x4 [px x? y? iw? ih?]
  (let [w (* px 4) h (* px 4)
        x (or x? 0) y (or y? 0)
        iw (or ih? h) ih (or iw? w)
        tab {}]
    (for [row 1 4]
      (for [col 1 4]
        (let [xo (+ x (* px (- col 1)))
              yo (+ x (* px (- row 1)))]
          (table.insert tab (love.graphics.newQuad xo yo px px iw ih)))))
    tab))

(fn nprint-map [x y image quad-list parsed-map px]
  (local lg love.graphics)
  (lg.push)
  (lg.translate x y)
  (each [row-number row (ipairs parsed-map)]
    (each [col-number value (ipairs row)]
      (let [quad (. quad-list value)
            xo (* px (- col-number 1))
            yo (* px (- row-number 1))]
        (lg.draw image quad xo yo))))
  (lg.pop))

(fn love.load [args]
  (lg.setDefaultFilter :nearest :nearest 0)
  (tset assets :water (lg.newImage :water.png))
  (tset assets :simplex (lg.newImage :simplex-noise-64.png))
  (tset assets :mask (lg.newImage :mask.png))
  (tset assets :cat (lg.newImage :cat.png))
  (tset assets :water-shader (lg.newShader :water.glsl))
  (tset assets :water-map (parse-map map))
  (tset assets :quads (make-quad-list-4x4 16))
  (tset assets :cat-quads [(love.graphics.newQuad 0 0 16 16 32 16)
                           (love.graphics.newQuad 16 0 16 16 32 16)])
  (when (assets.water-shader:hasUniform "simplex")
    (assets.water-shader:send "simplex" assets.simplex))
  (when (assets.water-shader:hasUniform "mask")
    (assets.water-shader:send "mask" assets.mask)))


(fn capture-gif [name step-count durration]
  (var step 0)
  (var time 0)
  (fn [dt]
    (set time (+ time dt))
    (when (> time durration)
      (do
        (set step (+ step 1))
        (if (> step step-count)
            true
            (do
              (set time 0)
              (love.graphics.captureScreenshot (.. name "~" step :.png))
              nil))))))

(local record-gif-flag true)
(local gif (capture-gif "water-step-6" 100 0.10))

(var time 0)
(var cat-quad-index 1)
(var cat-quad-timer 0)
(fn love.update [_dt]
  (local dt (if record-gif-flag 0.02 _dt))
  (set time (+ time dt))
  (set cat-quad-timer (+ cat-quad-timer dt))
  (when (> cat-quad-timer 0.6)
    (set cat-quad-timer 0)
    (incfb cat-quad-index 2))
  (when record-gif-flag (gif dt)))

(fn love.draw []
  (lg.push)
  (lg.scale scale)
  (lg.setShader assets.water-shader)
  (assets.water-shader:send "time" time)
  (nprint-map 0 0 assets.water assets.quads assets.water-map 16)
  (lg.draw assets.cat (. assets.cat-quads cat-quad-index) (* 16 3.5) (+ 32 (* 16 2.2)) 0 1 -1)
  (lg.setShader)
  (lg.draw assets.cat (. assets.cat-quads cat-quad-index) (* 16 3.5) (* 16 2.2))

  (lg.pop))
