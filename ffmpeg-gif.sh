directory=$HOME/.local/share/love/AlexJGriffith_Water_Shader
file="water-step-6~"

## Assumes there is only 1 ~ in the path
files=$(ls $directory/$file*png | sort -n -t~ -k1)
convert -layers Optimize -delay 10 $files output.gif
