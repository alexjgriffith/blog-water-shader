love.conf = function(t)
   t.gammacorrect = false
   t.title, t.identity = "AlexJGriffith_Water_Shader", "AlexJGriffith_Water_Shader"
   t.modules.joystick = false
   t.modules.physics = false
   t.window.width = 512
   t.window.height = 512
   t.window.vsync = true
   t.window.resizable = false
   t.version = "11.4"
end
